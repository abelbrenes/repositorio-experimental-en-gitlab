title: Report of vulnerability detected: Unsupported Windows OS (remote)
Severity: Critical
ID: 108797
version: 1.8
type: remote
keywords: windows, unsupported, 108797
last_updated: 04/23/2020
tags: [critical, incident]
summary: "Unsupported Windows OS (remote)"
sidebar: incidente_01
permalink: incidente_01.html
folder: incidentes-S.O.
conf: Public
lang: es

# Universidad de Costa Rica
## Centro de Informática [CI-UCR](https://ci.ucr.ac.cr)
### Unidad de Gestión de Riesgo y Seguridad 

```
Reporte de vulnerabilidad detectada en equipos con Sistema operativo: Windows
```

## Vulnerabilidad: Unsupported Windows OS (remote)
#### ID: 108797 - Severidad 1: CRÍTICA 
> El sistema operativo que se ejecuta en el host remoto ya no es compatible.
> 

### Problema
A la versión remota de Microsoft Windows le falta un paquete de servicio o ya no es compatible. Como resultado, es probable que contenga vulnerabilidades de seguridad.

### Solución
Actualice a una versión del sistema operativo Unix que sea compatible actualmente.

### Pasos a seguir 
1.  Ver también
https://support.microsoft.com/en-us/lifecycle

### Comentarios
Se recomienda la aplicación de acciones remediales de forma inmediata.

### Fuentes
Fuente: Tenable - Nessus Pro plugin:
[Report ID: https://www.tenable.com/plugins/nessus/108797](https://www.tenable.com/plugins/nessus/108797)


<!--stackedit_data:
eyJwcm9wZXJ0aWVzIjoidGl0bGU6IFJlcG9ydGUgZGUgaW5jaW
RlbnRlcyBkZSBzZWd1cmlkYWRcbmF1dGhvcjogJ1VuaWRhZCBk
ZSBHZXN0acOzbiBkZSBSaWVzZ28geSBTZWd1cmlkYWQsIENJLV
VDUidcbmRhdGU6ICcyMDIwLTA1LTA0J1xuc3RhdHVzOiBkcmFm
dFxudGFnczogJ1VSUywgQ0ksIFVDUiwgaW5jaWRlbnRlLCBzZW
d1cmlkYWQnXG5jYXRlZ29yaWVzOiAnU2VjdXJpdHksIGluY2lk
ZW50J1xuIiwiaGlzdG9yeSI6WzY5NzE1NTc5LC03NjU1OTE5Nz
ksLTI5MjQzMDYwMiwtMjA5Njk2NTc1NywyMDMzMDc3MiwxMzQ0
MDU5OTkwLDMwMzQyNTY0MSwxOTM5MzA3MjU5LC02OTA0NDc2OT
MsOTA5ODMwNjQwXX0=
-->