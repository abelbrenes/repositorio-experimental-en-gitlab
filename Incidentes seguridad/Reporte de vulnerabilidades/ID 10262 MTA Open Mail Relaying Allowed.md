title: Report of vulnerability detected about MTA Open Mail Relaying Allowed
ID: 10262
version: 1.63
Family: SMTP problems
keywords: smtp, unsupported, 10262
last_updated: 10/11/2019
tags: [critical, incident]
summary: "MTA Open Mail Relaying Allowed"
sidebar: incidente_01
permalink: incidente_01.html
folder: incidentes-S.O.
conf: Public
lang: es

# Universidad de Costa Rica
## Centro de Informática [CI-UCR](https://ci.ucr.ac.cr)
### Unidad de Gestión de Riesgo y Seguridad 

```
Reporte de vulnerabilidad detectada en el permiso para el MTA Open Mail Relaying
```

## Vulnerabilidad: MTA Open Mail Relaying Allowed
#### ID: 10262 - Severidad 1: CRÍTICA 
> Nessus ha detectado que el servidor SMTP remoto permite la retransmisión de correo.
> 

### Problema
Este problema permite que cualquier spammer use su servidor de correo para enviar su correo al mundo, inundando así el ancho de banda de su red y posiblemente haciendo que su servidor de correo aparezca en la lista negra.

### Solución
Vuelva a configurar su servidor SMTP para que no pueda usarse como un relay SMTP indiscriminado. Asegúrese de que el servidor utiliza los controles de acceso adecuados para limitar el alcance de la retransmisión.

### Pasos a seguir 
https://en.wikipedia.org/wiki/Email_spam

### Comentarios
Se recomienda la aplicación de acciones remediales de forma inmediata.

### Fuentes
Fuente: Tenable - Nessus Pro plugin:
[Report ID: https://www.tenable.com/plugins/nessus/10262](https://www.tenable.com/plugins/nessus/10262)

<!--stackedit_data:
eyJwcm9wZXJ0aWVzIjoidGl0bGU6IFJlcG9ydGUgZGUgaW5jaW
RlbnRlcyBkZSBzZWd1cmlkYWRcbmF1dGhvcjogJ1VuaWRhZCBk
ZSBHZXN0acOzbiBkZSBSaWVzZ28geSBTZWd1cmlkYWQsIENJLV
VDUidcbmRhdGU6ICcyMDIwLTA1LTA0J1xuc3RhdHVzOiBkcmFm
dFxudGFnczogJ1VSUywgQ0ksIFVDUiwgaW5jaWRlbnRlLCBzZW
d1cmlkYWQnXG5jYXRlZ29yaWVzOiAnU2VjdXJpdHksIGluY2lk
ZW50J1xuIiwiaGlzdG9yeSI6WzIwODgyNDc0NzQsLTIwNzM3ND
g2MDYsLTI4MjA5NjYzLC0xNTcwNjkxNDM5LDIwMzAwNzA1MSwt
NzcwMzkwMDE0LC04NDkyNDUyODAsNzM5MzMwODI0LC02ODkwNT
I4OTYsMTM5MjE2MzgyNSwtMTI5MjAzMzg5NSwtNTQ1MTM1Njkw
LDMwMzQyNTY0MSwxOTM5MzA3MjU5LC02OTA0NDc2OTMsOTA5OD
MwNjQwXX0=
-->