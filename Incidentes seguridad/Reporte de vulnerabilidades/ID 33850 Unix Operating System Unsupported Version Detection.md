title: Report of vulnerability detected about Unix Operating System Unsupported Version Detection
Severity: Critical
ID: 33850
version: 1.258
Family: General
keywords: unix, unsupported, 33850
last_updated: 03/30/2020
tags: [critical, incident]
summary: "Unix Operating System Unsupported Version Detection"
sidebar: incidente_01
permalink: incidente_01.html
folder: incidentes-S.O.
conf: Public
lang: es

# Universidad de Costa Rica
## Centro de Informática [CI-UCR](https://ci.ucr.ac.cr)
### Unidad de Gestión de Riesgo y Seguridad 

```
Reporte de vulnerabilidad detectada en equipos con Sistema operativo Unix
```

## Vulnerabilidad: Unix Operating System Unsupported Version Detection
#### ID: 33850 - Severidad 1: CRÍTICA 
> El sistema operativo que se ejecuta en el host remoto ya no es compatible.
> 
Según su número de versión auto-reportada, el sistema operativo Unix que se ejecuta en el host remoto ya no es compatible.

### Problema
La falta de soporte implica que el proveedor no lanzará nuevos parches de seguridad para el producto. Como resultado, es probable que contenga vulnerabilidades de seguridad.

### Solución
Actualice a una versión del sistema operativo Unix que sea compatible actualmente.

### Pasos a seguir 
1.  Ubuntu:
- Ubuntu 12.04 support ended on 2017-04-30. Upgrade to Ubuntu 19.10 / LTS 18.04 / LTS 16.04. For more information, see : https://wiki.ubuntu.com/Releases
- Ubuntu 12.10 support ended on 2014-05-16. Upgrade to Ubuntu 19.10 / LTS 18.04 / LTS 16.04. For more information, see : https://wiki.ubuntu.com/Releases
- Ubuntu 13.04 support ended on 2014-01-27. Upgrade to Ubuntu 19.10 / LTS 18.04 / LTS 16.04. For more information, see : https://wiki.ubuntu.com/Releases
- Ubuntu 10.04 support ended on 2013-05-09 (Desktop) / 2015-04-30 (Server). Upgrade to Ubuntu 19.10 / LTS 18.04 / LTS 16.04. For more information, see : https://wiki.ubuntu.com/Releases

2. Debian.
Debian 7.0 support ended on 2016-04-26 end of regular support / 2018-05-01 (end of long-term support for Wheezy-LTS). Upgrade to Debian Linux 9.x ("Stretch"). For more information, see : http://www.debian.org/releases/

- Fedora.
Fedora release 13 support ended on 2011-06-24. Upgrade to Fedora 30 / 31. For more information, see : https://fedoraproject.org/wiki/End_of_life
- Fedora release 14 support ended on 2011-12-08. Upgrade to Fedora 30 / 31. For more information, see : https://fedoraproject.org/wiki/End_of_life
- Fedora release 15 support ended on 2012-06-26. Upgrade to Fedora 30 / 31. For more information, see : https://fedoraproject.org/wiki/End_of_life .

### Comentarios
Se recomienda la aplicación de acciones remediales de forma inmediata.

### Fuentes
Fuente: [Tenable - Nessus Pro scanner report](plugin:
[Report ID: https://www.tenable.com/productlugins/nessus/nessus-professional)
33850](https://www.tenable.com/plugins/nessus/33850)

<!--stackedit_data:
eyJwcm9wZXJ0aWVzIjoidGl0bGU6IFJlcG9ydGUgZGUgaW5jaW
RlbnRlcyBkZSBzZWd1cmlkYWRcbmF1dGhvcjogJ1VuaWRhZCBk
ZSBHZXN0acOzbiBkZSBSaWVzZ28geSBTZWd1cmlkYWQsIENJLV
VDUidcbmRhdGU6ICcyMDIwLTA1LTA0J1xuc3RhdHVzOiBkcmFm
dFxudGFnczogJ1VSUywgQ0ksIFVDUiwgaW5jaWRlbnRlLCBzZW
d1cmlkYWQnXG5jYXRlZ29yaWVzOiAnU2VjdXJpdHksIGluY2lk
ZW50J1xuIiwiaGlzdG9yeSI6WzY4NzA0NTg3MywtNzg5MTc1ND
M3LDMwMzQyNTY0MSwxOTM5MzA3MjU5LC02OTA0NDc2OTMsOTA5
ODMwNjQwXX0=
-->