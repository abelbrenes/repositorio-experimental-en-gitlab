title: Report of vulnerability detected about Microsoft IIS 6.0 Unsupported Version Detection
Severity: Critical
ID: 97994
version: $Revision: 1.1 $
Family: Web Servers
keywords: windows, unsupported, 97994
last_updated: 04/17/2017
tags: [critical, incident]
summary: "Microsoft IIS 6.0 Unsupported Version Detection"
sidebar: incidente_01
permalink: incidente_01.html
folder: incidentes-S.O.
conf: Public
lang: es

# Universidad de Costa Rica
## Centro de Informática [CI-UCR](https://ci.ucr.ac.cr)
### Unidad de Gestión de Riesgo y Seguridad 

```
Reporte de vulnerabilidad detectada en Microsoft IIS 6.0 versión no soportada
```

## Vulnerabilidad: Microsoft IIS 6.0 Unsupported Version Detection
#### ID: 97994 - Severidad 1: CRÍTICA 
> De acuerdo al su número de versión autoinformado, ya no se admite la instalación de Microsoft Internet Information Services (IIS) 6.0 en el host remoto.
> 

### Problema
La falta de soporte implica que el proveedor no lanzará nuevos parches de seguridad para el producto. Como resultado, es probable que contenga vulnerabilidades de seguridad

### Solución
Actualice a una versión de Microsoft IIS que sea compatible actualmente.

### Pasos a seguir 
http://www.nessus.org/u?d99a8431
https://www.microsoft.com/en-us/cloud-platform/windows-server-2003

### Comentarios
Se recomienda la aplicación de acciones remediales de forma inmediata.

### Fuentes
Fuente: Tenable - Nessus Pro plugin:
[Report ID: https://www.tenable.com/plugins/nessus/97994](https://www.tenable.com/plugins/nessus/97994)

<!--stackedit_data:
eyJwcm9wZXJ0aWVzIjoidGl0bGU6IFJlcG9ydGUgZGUgaW5jaW
RlbnRlcyBkZSBzZWd1cmlkYWRcbmF1dGhvcjogJ1VuaWRhZCBk
ZSBHZXN0acOzbiBkZSBSaWVzZ28geSBTZWd1cmlkYWQsIENJLV
VDUidcbmRhdGU6ICcyMDIwLTA1LTA0J1xuc3RhdHVzOiBkcmFm
dFxudGFnczogJ1VSUywgQ0ksIFVDUiwgaW5jaWRlbnRlLCBzZW
d1cmlkYWQnXG5jYXRlZ29yaWVzOiAnU2VjdXJpdHksIGluY2lk
ZW50J1xuIiwiaGlzdG9yeSI6Wzc2NzUzOTYwOSwtMTU3MDY5MT
QzOSwyMDMwMDcwNTEsLTc3MDM5MDAxNCwtODQ5MjQ1MjgwLDcz
OTMzMDgyNCwtNjg5MDUyODk2LDEzOTIxNjM4MjUsLTEyOTIwMz
M4OTUsLTU0NTEzNTY5MCwzMDM0MjU2NDEsMTkzOTMwNzI1OSwt
NjkwNDQ3NjkzLDkwOTgzMDY0MF19
-->