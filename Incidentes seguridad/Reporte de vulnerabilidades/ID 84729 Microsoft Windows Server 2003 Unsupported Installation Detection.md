title: Report of vulnerability detected about Microsoft Windows Server 2003 Unsupported Installation Detection
Severity: Critical
ID: 84729
version: 1.11
Family: Windows
keywords: windows, unsupported, 84729
last_updated: 11/15/2018
tags: [critical, incident]
summary: "Microsoft Windows Server 2003 Unsupported Installation Detection"
sidebar: incidente_01
permalink: incidente_01.html
folder: incidentes-S.O.
conf: Public
lang: es

# Universidad de Costa Rica
## Centro de Informática [CI-UCR](https://ci.ucr.ac.cr)
### Unidad de Gestión de Riesgo y Seguridad 

```
Reporte de vulnerabilidad detectada en Microsoft Windows Server 2003 instalación no soportada
```

## Vulnerabilidad: Microsoft Windows Server 2003 Unsupported Installation Detection
#### ID: 84729 - Severidad 1: CRÍTICA 
> El host remoto ejecuta Microsoft Windows Server 2003. El soporte para este sistema operativo por parte de Microsoft finalizó el 14 de julio de 2015.
> 

### Problema
La falta de soporte implica que el proveedor no lanzará nuevos parches de seguridad para el producto. Como resultado, es probable que contenga vulnerabilidades de seguridad. Además, es poco probable que Microsoft investigue o reconozca los informes de vulnerabilidades.

### Solución
Actualice a una versión de Windows que sea compatible actualmente.

### Pasos a seguir 
Actualice a una versión de Windows que sea compatible actualmente.

### Comentarios
Se recomienda la aplicación de acciones remediales de forma inmediata.

### Fuentes
Fuente: Tenable - Nessus Pro plugin:
[Report ID: https://www.tenable.com/plugins/nessus/84729](https://www.tenable.com/plugins/nessus/84729)

<!--stackedit_data:
eyJwcm9wZXJ0aWVzIjoidGl0bGU6IFJlcG9ydGUgZGUgaW5jaW
RlbnRlcyBkZSBzZWd1cmlkYWRcbmF1dGhvcjogJ1VuaWRhZCBk
ZSBHZXN0acOzbiBkZSBSaWVzZ28geSBTZWd1cmlkYWQsIENJLV
VDUidcbmRhdGU6ICcyMDIwLTA1LTA0J1xuc3RhdHVzOiBkcmFm
dFxudGFnczogJ1VSUywgQ0ksIFVDUiwgaW5jaWRlbnRlLCBzZW
d1cmlkYWQnXG5jYXRlZ29yaWVzOiAnU2VjdXJpdHksIGluY2lk
ZW50J1xuIiwiaGlzdG9yeSI6WzE4MjQ3MzE5NzIsLTc3MDM5MD
AxNCwtODQ5MjQ1MjgwLDczOTMzMDgyNCwtNjg5MDUyODk2LDEz
OTIxNjM4MjUsLTEyOTIwMzM4OTUsLTU0NTEzNTY5MCwzMDM0Mj
U2NDEsMTkzOTMwNzI1OSwtNjkwNDQ3NjkzLDkwOTgzMDY0MF19

-->