title: Report of vulnerability detected about PHP Unsupported Version Detection
Severity: Critical
ID: 58987
version: 1.17
Family: CGI abuses
keywords: php, unsupported, 58987
last_updated: 2020/05/21
tags: [critical, incident]
summary: "PHP Unsupported Version Detection"
sidebar: incidente_01
permalink: incidente_01.html
folder: incidentes-S.O.
conf: Public
lang: es

# Universidad de Costa Rica
## Centro de Informática [CI-UCR](https://ci.ucr.ac.cr)
### Unidad de Gestión de Riesgo y Seguridad 

```
Reporte de vulnerabilidad detectada en PHP
```

## Vulnerabilidad: PHP Unsupported Version Detection
#### ID: 58987 - Severidad 1: CRÍTICA 
> According to its version, the installation of PHP on the remote host is no longer supported.
> 

### Problema
La falta de soporte implica que el proveedor no lanzará nuevos parches de seguridad para el producto. Como resultado, es probable que contenga vulnerabilidades de seguridad.

### Solución
Actualice a una versión del sistema operativo Unix que sea compatible actualmente.

### Pasos a seguir 
Actualice a una versión de PHP que sea actualmente compatible.

### Comentarios
Se recomienda la aplicación de acciones remediales de forma inmediata.

### Fuentes
Fuente: Tenable - Nessus Pro plugin:
[Report ID: https://www.tenable.com/plugins/nessus/58987](https://www.tenable.com/plugins/nessus/58987)

<!--stackedit_data:
eyJwcm9wZXJ0aWVzIjoidGl0bGU6IFJlcG9ydGUgZGUgaW5jaW
RlbnRlcyBkZSBzZWd1cmlkYWRcbmF1dGhvcjogJ1VuaWRhZCBk
ZSBHZXN0acOzbiBkZSBSaWVzZ28geSBTZWd1cmlkYWQsIENJLV
VDUidcbmRhdGU6ICcyMDIwLTA1LTA0J1xuc3RhdHVzOiBkcmFm
dFxudGFnczogJ1VSUywgQ0ksIFVDUiwgaW5jaWRlbnRlLCBzZW
d1cmlkYWQnXG5jYXRlZ29yaWVzOiAnU2VjdXJpdHksIGluY2lk
ZW50J1xuIiwiaGlzdG9yeSI6Wzg2ODg0NDU3NiwtMTI5MjAzMz
g5NSwtNTQ1MTM1NjkwLDMwMzQyNTY0MSwxOTM5MzA3MjU5LC02
OTA0NDc2OTMsOTA5ODMwNjQwXX0=
-->