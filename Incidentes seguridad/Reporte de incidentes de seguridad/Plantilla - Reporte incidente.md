# Universidad de Costa Rica
## Centro de Informática 

# REPORTE DE INCIDENTE DE SEGURIDAD

## No. Incidente: URS-IS-<año-mes-día-secuencia>
Fecha: <fecha del reporte>
Hora: <hora del reporte>
Responsable del reporte: <Nombre y puesto del elaborador del reporte>
Correo electrónico: <del elaborador las acciones>

Unidad afectada: <área, unidad o departamento afectado>
Jefe de oficina: <jefe, director, coordinador unidad afectada>
Correo Institucional: <indicar>
Teléfono: <número>
RID de contacto: <Encargado de recursos informáticos>
Correo Institucional: <indicar>
Teléfono: <número>
Descripción del incidente: <Resumen general del incidente>
Causal del incidente: <Resumen general de las causas del evento>
Impacto: <Plataformas, equipos o servicios afectados por el evento>
#### Estado del incidente: <abierto, cerrado, pendiente de atención>
#### Acciones pendientes: <actividades pendientes de ejecutar>

Antecedentes: <describa detalladamene el evento incluyendo, horas de las actividades y avance>
Reporte elaborado por: <nombre de quien elabora el reporte en URS>
Correo Institucional: <indicar de eloaborador>

## ACCIONES REMEDIALES PARA MITIGAR EL INCIDENTE
Fecha: <fecha del reporte remedial>
Hora: <hora del reporte>
Responsable de las recomendaciones: <encargado de aplicar las acciones remediales>
Envía al correo electrónico a: < del encargado mitigar>
Descripción de las acciones: <Acciones detallas ejecutadas para mitigar el incidente>
Plazo para aplicación de acciones: <indicar tiempo estimado>
Duración: <Cant. horas>

## REPORTE DE MITIGACIÓN DEL ÁREA QUE ATENDIÓ EL INCIDENTE 
Fecha: <fecha del reporte de mitigación>
Hora: <hora del reporte>
Responsable del reporte: <encargado(s) de aplicar las acciones de mitigación del área>
Correo electrónico: < del encargado del área de mitigar>
Enviado por correo institucional  a: <encargado(s) quien recibe el informe>
Correo institucional: < del encargado del área de mitigar>
Detalle de acciones de mitigación: <Enumere las acciones aplicadas por el área>
Se mitigó el incidente (si/no) explique: <indique si se logró mitigar el incidente, justificando su respuesta>

### Contacto
Unidad de Gestión de riesgo y seguridad
[ursci@ucr.ac.cr](mailto: ursci@ucr.ac.cr)

> Written with [StackEdit](https://stackedit.io/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbNzg0ODM1MjY5LDQ4MTcyMjU0Nyw4MDU2NT
E5ODQsLTEwNzEwMTUyNjcsMTY5Mzg3NTU2MiwtNDAyMzIzMDQ1
LC0xNDgzNjE0Nzg5LC01NTU5OTg5ODQsLTEzODU3NzcxNTIsNz
MwOTk4MTE2XX0=
-->