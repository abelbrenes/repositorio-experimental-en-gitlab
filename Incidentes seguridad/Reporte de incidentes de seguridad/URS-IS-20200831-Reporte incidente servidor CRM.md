# Universidad de Costa Rica
## Centro de Informática 

# REPORTE DE INCIDENTE DE SEGURIDAD

## No. Incidente: URS-IS-20200831>
Fecha: 24/08/2020
Hora: 12:14 

Responsable del reporte: Sergio Vargas, Unidad de Riesgo y Seguridad (URS), CI-UCR
Correo electrónico: sergio.vargas@ucr.ac.cr

Unidad afectada: Área de Gestión del Usuario (AGU), Centro de Informática

Jefe de oficina: M.Sc. Alonso Castro M.
Correo Institucional: alonso.castro@ucr.ac.cr
Teléfono: 2511-2881

RID de contacto: Ing. Hellen Cubero
Correo Institucional: helen.cubero@ucr.ac.cr
Teléfono: 2511-1840

Descripción del incidente: El servidor de Nessus, detectó varias  vulnerabilidades, calificando 0 Críticas, 0 Altas, 5 Medio, 1 Baja, 97 Informativas, para un total de 103 vulnerabilidades
### Report Summary

### Plugins: Top 5

- Medium [51192](https://www.tenable.com/plugins/nessus/51192) SSL Certificate Cannot Be Trusted Medium [42873](https://www.tenable.com/plugins/nessus/42873) SSL

- Medium Strength Cipher Suites Supported (SWEET32)

- Medium [65821](https://www.tenable.com/plugins/nessus/65821) SSL RC4 Cipher Suites Supported (Bar Mitzvah)

- Medium [57582](https://www.tenable.com/plugins/nessus/57582) SSL Self-Signed Certificate

- Medium [104743](https://www.tenable.com/plugins/nessus/104743) TLS Version 1.0 Protocol Detection
Causal del incidente: Recomendación de un nuevo del certificado en el equipo.
Impacto: Pérdida de confianza de los usuarios y la afectación de la imagen, al conocer el mensaje de error de certificado de seguridad del servidor, que no puede ser verificada por el navegador web.
#### Estado del incidente: cerrado
#### Acciones pendientes: No hay pendientes.

Antecedentes:   
-   La Unidad de Riesgos y Seguridad (URS), decide realizar escaneos en el segmento de red de equipos virtuales en cuya plataforma está a cargo del Área de Gestión de Servicios (AGS), a través de la herramienta Nessus Professional.
    
-   La encargada del servidor X.X.170.59, Hellen Cubero, realiza las recomendaciones indicadas en las vulnerabilidades.

Reporte elaborado por: Sergio Vargas, Unidad de Riesgo y Seguridad, CI.
Correo Institucional: sergio.vargas@ucr.ac.cr

## ACCIONES REMEDIALES PARA MITIGAR EL INCIDENTE
Fecha: 31/08/2020
Hora: 12:00 m.d.

Responsable de las recomendaciones: Hellen Cubero
Envía al correo electrónico a: helen.cubero@ucr.ac.cr
Descripción de las acciones: 
1.  El Área de Gestión del Usuario (AGU), realiza la reinstalación del certificado SSL siguiendo la guía de pasos  de instalación, ubicada en el sitio web (apuntes.ucr.ac.cr), ubicada en el enlace: [http://apuntes.ucr.ac.cr/index.php/Sitios_seguros_HTTPS_con_Let%E2%80%99s_Encrypt](http://apuntes.ucr.ac.cr/index.php/Sitios_seguros_HTTPS_con_Let%E2%80%99s_Encrypt),

2. Posteriormente a la instalación del certificado SSL, la encarga del equipo, Hellen Cubero, realiza por iniciativa un análisis por medio de la herramienta Qualys, luego de actualizar el certificado SSL
Plazo para aplicación de acciones: A la brevedad
Duración: 10 minutos

## REPORTE DE MITIGACIÓN DEL ÁREA QUE ATENDIÓ EL INCIDENTE 
Fecha: 21/08/2020
Hora: 13:14

Responsable del reporte: Hellen Cubero
Correo electrónico: helen.cubero@ucr.ac.cr

Enviado por correo institucional  a: Hellen Cubero
Correo institucional: helen.cubero@ucr.ac.cr

Detalle de acciones de mitigación: 

Las recomendaciones en certificados SSL en servidores como 163.178.170.59 (se recomendó aplicar un nuevo certificado) han sido revisadas. El certificado SSL fue creado con cerbot, el cual utiliza Let's Encrypt como autoridad de certificación.  
Se realizó un test a los certificados creados e indicaron calificación "A" y actualmente siguen siendo bien calificados (aunque comprendemos que esto podría diferir de lo indicado por Nessus, porque son herramientas distintas):

[Imagen: Qualys SSL Report] 

También utilizamos el sitio [https://ssl-config.mozilla.org/](https://ssl-config.mozilla.org/) para aplicar configuraciones de seguridad en el archivo vhost, de acuerdo con el servidor web utilizado.

[Imagen: Mozilla SSL configurator generator] 

Se mitigó el incidente (si/no) explique: SI

### Contacto. 
Escribamos a Soporte del repositorio de Incidentes, Unidad de Gestión de riesgo y seguridad
[ursci@ucr.ac.cr]

[Imagen: Qualys SSL Report]: https://gitlab.com/abelbrenes/repositorio-experimental-en-gitlab/-/tree/master/Incidentes%20seguridad/Reporte%20de%20incidentes%20de%20seguridad/images/URS-IS-20200831-Qualys-ssl-labs.png "Qualys-ssl-labs"

[Imagen: Mozilla SSL configurator generator]: https://gitlab.com/abelbrenes/repositorio-experimental-en-gitlab/-/tree/master/Incidentes%20seguridad/Reporte%20de%20incidentes%20de%20seguridad/images/URS-IS-20200831-SSL-configuration-generator.png "SSL configurator generator"